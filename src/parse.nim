import strutils

type
  GopherKind* {.pure.} = enum
    Invalid, Redundant, End, Txt, Dir, Ccso,
    Error, BinHex, Dos, Unix, IndexServ, Telnet,
    Bin, Image, Tn3270, Gif, Html, Info, Sound
  
  GopherLine* = ref object
    case kind*: GopherKind
    of Redundant, Txt, Dir, Error, BinHex, Dos, Unix,
      IndexServ, Bin, Image, Gif, Html, Info, Sound:
      displayStr*, selector*, hostname*, port*: string
    else:
      discard

proc gopherKind(c: char): GopherKind =
  case c:
    of '+': Redundant
    of '.': End
    of '0': Txt
    of '1': Dir
    of '2': Ccso
    of '3': Error
    of '4': BinHex
    of '5': Dos
    of '6': Unix
    of '7': IndexServ
    of '8': Telnet
    of '9': Bin
    of 'I': Image
    of 'T': Tn3270
    of 'g': Gif
    of 'h': Html
    of 'i': Info
    of 's': Sound
    else: Invalid

template hasContent(k: GopherKind): bool =
  case k:
    of Redundant, Txt, Dir, Error, BinHex, Dos, Unix,
      IndexServ, Bin, Image, Gif, Html, Info, Sound:
      true
    else:
      false

proc parseGopher*(s: string): GopherLine =
  result = new(GopherLine)
  if s.len < 1:
    result.kind = Invalid
  else:
    var kind = gopherKind(s[0])
    if kind.hasContent:
      let spl = s.split('\t')
      if spl.len < 4:
        result.kind = Invalid
      else:
        result.kind = kind
        result.displayStr = spl[0][1..high(spl[0])]
        result.selector = spl[1]
        result.hostname = spl[2]
        result.port = spl[3]
