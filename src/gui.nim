import asyncdispatch, asyncfutures
import sdl2, sdl2/ttf
import private/entrybox

type SDLException = object of Exception

type
  LineKind {.pure.} = enum
    Text, Link, Box

  CacheLine = object
    case kind: LineKind
    of Text:
      discard
    of Link:
      dest: string
      onClick: proc(s: string)
    of Box:
      entryBox: EntryBox
      onSubmit: proc(s: string)
    w, h: cint
    label: TexturePtr

  ActionKind* {.pure.} = enum
    Quit, BarOpen, BarClose, BarSubmit,
    Back, Forward, WinChg
  
  Action* = object
    case kind*: ActionKind
    of BarSubmit:
      contents: string
    else:
      discard
  

var
  window: WindowPtr
  renderer: RendererPtr
  font: FontPtr
  lineCache: seq[CacheLine] = @[]
  showBar = false
  barContents = ""
  ctrlPressed = false
  shiftPressed = false
  renderLinePos = 0


template sdlFailIf(cond: typed, reason: string) =
  if cond: raise SDLException.newException(
    reason & ", SDL error: " & $getError())


proc guiInit* =
  sdlFailIf(not sdl2.init(INIT_VIDEO or INIT_TIMER or INIT_EVENTS)):
    "SDL2 initialization failed"
    

  sdlFailIf(ttfInit() == SdlError):
    "SDL2 TTF initialization failed"

  # defer blocks get called at the end of the procedure, even if an
  # exception has been thrown
  sdlFailIf(not setHint("SDL_RENDER_SCALE_QUALITY", "2")):
    "Linear texture filtering could not be enabled"

  window = createWindow(title = "Goldy",
    x = SDL_WINDOWPOS_CENTERED, y = SDL_WINDOWPOS_CENTERED,
    w = 800, h = 600, flags = SDL_WINDOW_SHOWN)
  sdlFailIf window.isNil: "Window could not be created"

  renderer = window.createRenderer(index = -1,
    flags = Renderer_Accelerated or Renderer_PresentVsync)
  sdlFailIf renderer.isNil: "Renderer could not be created"

  renderer.setDrawColor(r = 240, g = 240, b = 240)
  font = openFont("res/IBMPlexMono-Regular.ttf", 14)


proc guiExit* =
  window.destroy()
  renderer.destroy()
  ttfQuit()
  sdl2.quit()


proc newCacheText(text: string, color: Color): CacheLine =
  let surface = font.renderUtf8Blended(text.cstring, color)
  sdlFailIf surface.isNil: "Could not render text surface"

  discard surface.setSurfaceAlphaMod(color.a)

  let texture = renderer.createTextureFromSurface(surface)

  sdlFailIf texture.isNil:
    "Could not create texture from rendered text"

  result = CacheLine(kind: Text, w: surface.w, h: surface.h, label: texture)
  surface.freeSurface()


proc newCacheEntryBox(label, text: string,
                         handle: proc(s: string)): CacheLine =
  let surface = font.renderUtf8Blended(label.cstring, color(0, 0, 0, 255))
  sdlFailIf surface.isNil: "Could not render text surface"

  let texture = renderer.createTextureFromSurface(surface)
  let box = EntryBox(contents: "", activepos: 0)
  
  result = CacheLine(kind: Box, entryBox: box,
                     w: surface.w, h: surface.h, label: texture, onSubmit: handle)
  surface.freeSurface()


proc redraw*(line: int = renderLinePos) =
  renderLinePos =
    if line < high(lineCache): line
    else: high(lineCache)

  renderer.clear()
  var y = 2
  if len(lineCache) > 0:
    for l in lineCache[renderLinePos..high(lineCache)]:
      var
        source = rect(0, 0, l.w, l.h)
        dest = rect(2, y.cint, l.w, l.h)
      case l.kind:
        of Text:
          renderer.copyEx(l.label, source, dest, angle = 0.0, center = nil,
                          flip = SDL_FLIP_NONE)
        of Box:
          renderer.copyEx(l.label, source, dest, angle = 0.0, center = nil,
                          flip = SDL_FLIP_NONE)
          renderer.draw(l.entryBox, 4 + l.w, y.cint, 100, l.h, font)
        of Link:
          discard
      y += 16

  if showBar:
    var ebox = rect(0, 0, 150, 20)
    renderer.setDrawColor(r = 120, g = 120, b = 120)
    renderer.fillRect(ebox)
    renderer.setDrawColor(r = 240, g = 240, b = 240)
  renderer.present()


proc clearBuffer* =
  for l in lineCache:
    l.label.destroy()
  lineCache = @[]


proc addText*(text: string, fill: Color = color(0, 0, 0, 255)) =
  lineCache.add newCacheText(text, fill)


proc addEntry*(label: string, handle: proc(s: string)) =
  lineCache.add newCacheEntryBox(label, "", handle)


proc setBar*(en: bool) =
  showBar = en


proc setBar*(en: bool, text: string) =
  showBar = en
  barContents = text


proc handleInput*(): seq[Action] =
  var
    event = defaultEvent
  result = @[]
  echo "calling handle"
  while pollEvent(event):
    case event.kind:
      of QuitEvent:
        result = @[Action(kind: Quit)]
        break
      
      of KeyDown:
#         echo event.key.keysym.sym.char, " ", event.key.keysym.scancode, " down"
        let sc = event.key.keysym.scancode
        if sc == SDL_SCANCODE_LCTRL or sc == SDL_SCANCODE_RCTRL:
          ctrlPressed = true
        elif sc == SDL_SCANCODE_LSHIFT or sc == SDL_SCANCODE_RSHIFT:
          shiftPressed = true
#         elif sc.isPrintable:
          
#           of SDL_SCANCODE_LCTRL, SDL_SCANCODE_RCTRL:
#             ctrlPressed = true
#           
#           of SDL_SCANCODE_HOME:
#             renderLinePos = 0
#           of SDL_SCANCODE_END:
#             renderLinePos = high(lineCache)
      
      of KeyUp:
#         echo event.key.keysym.sym.char, " ", event.key.keysym.scancode, " up"
        let sc = event.key.keysym.scancode
        if sc == SDL_SCANCODE_LCTRL or sc == SDL_SCANCODE_RCTRL:
          ctrlPressed = false
        elif sc == SDL_SCANCODE_LSHIFT or sc == SDL_SCANCODE_RSHIFT:
          shiftPressed = false
        else:
          discard
      # of MouseWheel:
      # of MouseButtonDown:
      # 
      of WindowEvent:
        case event.window.event
        of WindowEvent_Resized, WindowEvent_Restored,
           WindowEvent_Maximized, WindowEvent_SizeChanged:
          result.add Action(kind: WinChg)
        else:
          discard
      else:
        discard

