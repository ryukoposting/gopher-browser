import asyncnet, asyncdispatch
import parse, gui, sdl2

type
  InputKind {.pure.} = enum
    Quit, Query, None


proc execrequest(): Future[seq[GopherLine]] {.async.} =
  result = @[]
  var sock = newAsyncSocket()
  echo("connecting...")
  waitFor sock.connect("gopher.floodgap.com", Port(70))
  echo("connected.")
  waitFor sock.send("\r\n")
  var ln = await sock.recvLine()
  while ln != "":
    result.add ln.parseGopher
    ln = await sock.recvLine()
  sock.close()


proc printThings(): Future[int] {.async.} =
  while true:
    echo "hello world"
    await sleepAsync 500
  result = 2


proc main {.async.} =
  while true:
    discard handleInput()
    redraw()
    await sleepAsync 16

guiInit()
waitFor main()

# addText "hello, world!"
# addEntry "V2:", proc(s: string) =
#   discard
# setBar on
# while true: discard
guiExit()

