# Package

version       = "0.1.0"
author        = "ryukoposting"
description   = "A Gopher Browser"
license       = "Apache-2.0"
srcDir        = "src"
bin           = @["gopher"]


# Dependencies

requires "nim >= 0.19.2"
requires "sdl2"
